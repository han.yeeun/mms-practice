import React from "react";

const VideoComponent = () => {
  return (
    <div className="relative w-full h-50">
      <video className="w-full h-90" controls>
        {/* <source
          src="https://www.w3schools.com/html/mov_bbb.mp4"
          type="video/mp4"
        /> */}
      </video>
      <div className="absolute top-0 bg-black bg-opacity-50 text-white w-full text-center h-6">
        <h1 className="text-m font-bold">name</h1>
      </div>
    </div>
  );
};

export default VideoComponent;

import React, { useState } from "react";
import "../index.css";
import ButtonComponent from "./button";
import VideoComponent from "./video";
import VideoStream from "./VideoStream";

const OtherPage = () => {
  const [toggle1, setToggle1] = useState(false);
  const [toggle2, setToggle2] = useState(true); // 초기 상태를 true로 설정
  const [toggle3, setToggle3] = useState(false);

  const handleToggle1 = () => {
    setToggle1(!toggle1);
  };

  const handleToggle2 = () => {
    setToggle2(!toggle2);
  };

  const handleToggle3 = () => {
    setToggle3(!toggle3);
  };

  const [isHovered, setIsHovered] = useState(false);

  return (
    <div className="App min-h-screen bg-black flex items-center justify-center ">
      <div>
        <div className="grid grid-cols-3 gap-1 h-screen ">
          <div className="text-4xl font-bold ml-6 text-white">
            MMS 관리자 모드
          </div>
          <div className=""></div>
          <div className="flex justify-end mr-6">
            <ButtonComponent />
          </div>
          <div
            className="col-span-2 grid gap-1 ml-6 w-full h-full relative"
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
          >
            <div className="grid gap-1 overflow-y-auto h-full">
              <div className="relative w-full h-full">
                <VideoStream isCameraOn={toggle2} />
                <div className="absolute top-0 bg-black bg-opacity-50 text-white w-full text-center h-6">
                  <h1 className="text-m font-bold">
                    0000호 홍길동 환자의 모습
                  </h1>
                </div>
                {isHovered && (
                  <div className="absolute bottom-0 bg-black bg-opacity-50 text-white w-full text-center h-1/3 flex justify-center items-center ">
                    <button className="text-white mr-2" onClick={handleToggle1}>
                      음소거: {toggle1 ? "ON" : "OFF"}
                    </button>
                    <button className="text-white mr-2" onClick={handleToggle2}>
                      카메라: {toggle2 ? "ON" : "OFF"}
                    </button>
                    <button className="text-white" onClick={handleToggle3}>
                      공유: {toggle3 ? "ON" : "OFF"}
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="grid gap-1 mr-6 h-30 overflow-y-auto min-h-60">
            <div className="">
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
            </div>
          </div>
          <div>
            <div className="col-span-1 grid gap-1 ml-6 w-5xl">
              <div className="grid  gap-1">
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OtherPage;

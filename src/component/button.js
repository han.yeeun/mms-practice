import React from "react";

export default function ButtonComponent() {
  const click = () => {
    const confirmed = window.confirm("정말 나가겠습니까?");
    if (confirmed) {
      window.location.href = "https://dogutest.dogong.xyz/main";
    }
  };

  return (
    <div>
      <button
        onClick={click}
        className="bg-red-500 text-white px-4 py-2 rounded font-bold text-xl my-1"
      >
        나가기
      </button>
    </div>
  );
}

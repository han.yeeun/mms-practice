import React, { useState, useRef, useEffect } from "react";
import Webcam from "react-webcam";

const VideoStream = ({ isCameraOn }) => {
  const webcamRef = useRef(null);

  useEffect(() => {
    if (!isCameraOn && webcamRef.current) {
      const stream = webcamRef.current.stream;
      if (stream) {
        stream.getTracks().forEach((track) => track.stop());
      }
    }
  }, [isCameraOn]);

  return (
    <div className="w-full h-full">
      {isCameraOn && (
        <Webcam
          audio={false}
          ref={webcamRef}
          width="100%"
          height="100%"
          screenshotFormat="image/jpeg"
          videoConstraints={{
            width: 1280,
            height: 720,
            facingMode: "user",
          }}
        />
      )}
    </div>
  );
};

export default VideoStream;

import React, { useState } from "react";
import "../index.css";
import VideoComponent from "./video";
import ButtonComponent from "./button";
import VideoStream from "./VideoStream";

function App() {
  const [toggle1, setToggle1] = useState(false); // 음소거 상태
  const [toggle2, setToggle2] = useState(true); // 카메라 상태

  const handleToggle1 = () => {
    setToggle1(!toggle1);
  };

  const handleToggle2 = () => {
    setToggle2(!toggle2);
  };

  const [isHovered, setIsHovered] = useState(false);

  return (
    <div className="App min-h-screen bg-black flex items-center justify-center">
      <div>
        <div className="grid grid-cols-3 gap-1 h-screen">
          <div className="text-4xl font-bold ml-6 text-white">DOGU MMS</div>
          <div></div>
          <div className="flex justify-end mr-6">
            <ButtonComponent />
          </div>
          <div className="col-span-2 grid gap-1 ml-6 w-full h-full relative">
            <div className="grid gap-1 overflow-y-auto h-full">
              <div className="relative w-full h-full">
                <VideoStream isCameraOn={toggle2} isMuted={toggle1} />
                <div className="absolute top-0 bg-black bg-opacity-50 text-white w-full text-center h-6">
                  <h1 className="text-m font-bold">
                    0000호 홍길동 환자의 모습
                  </h1>
                </div>
                {isHovered && (
                  <div className="absolute bottom-0 bg-black bg-opacity-50 text-white w-full text-center h-1/3 flex justify-center items-center">
                    <button className="text-white mr-2" onClick={handleToggle1}>
                      음소거: {toggle1 ? "ON" : "OFF"}
                    </button>
                    <button className="text-white mr-2" onClick={handleToggle2}>
                      카메라: {toggle2 ? "ON" : "OFF"}
                    </button>
                  </div>
                )}
              </div>
            </div>

            <div className="grid gap-1 overflow-y-auto h-full">
              <div className="relative w-full h-full">
                <VideoStream isCameraOn={toggle2} isMuted={toggle1} />
                <div className="absolute top-0 bg-black bg-opacity-50 text-white w-full text-center h-6">
                  <h1 className="text-m font-bold bg-zinc-300">의사의 모습</h1>
                </div>
              </div>
            </div>
          </div>
          <div className="grid gap-1 mr-6 h-30 overflow-y-auto min-h-60">
            <div>
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
              <VideoComponent />
            </div>
          </div>
          <div>
            <div className="col-span-1 grid gap-1 ml-6 w-5xl">
              <div className="grid gap-1">
                <div></div>
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;

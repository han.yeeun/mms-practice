import React from "react";
import "./index.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AllComponent from "./component/All";
import OtherPage from "./component/OtherPage";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<AllComponent />} />
        <Route path="/other-page" element={<OtherPage />} />
      </Routes>
    </Router>
  );
}

export default App;
